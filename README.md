# Cisco Packet Tracer AppImage

### Download

- you can get from [releases](https://gitlab.com/fr0stb1rd/cisco-packet-tracer-appimage/-/releases) (`Cisco.Packet.Tracer.AppImage.vXXX.7z`)

### Usage

- [fr0stb1rd.gitlab.io/posts/Cisco-Packet-Tracer-AppImage/](https://fr0stb1rd.gitlab.io/posts/Cisco-Packet-Tracer-AppImage/)

### Info

- added `.gitlab-ci.yml` for build.
- built with ubuntu:latest.
- update code as what you want, create tag like `8.3.1`.
    - CI automatically build & upload & release it for you. (I spent a day writing this.)

### Credits

- CI & autobuild & releases: [fr0stb1rd/cisco-packet-tracer-appimage](https://gitlab.com/fr0stb1rd/cisco-packet-tracer-appimage)
- main code: [konradmb/PacketTracer-AppImage](https://github.com/konradmb/PacketTracer-AppImage)
